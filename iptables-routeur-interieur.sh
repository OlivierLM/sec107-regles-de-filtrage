#!/bin/bash

# Mon identifiant
MY_ID=0

### Tout Remettre à zéro
iptables -t nat -F
iptables -F
### Accepter les messages ICMP en entrée
iptables -A INPUT -p ICMP -j ACCEPT
### Tout accepter sur l'interface locale
iptables -A INPUT -i lo -j ACCEPT
### Suivi de connexions (stateful firewall)
# Sur les deux chaînes :
# - INPUT : paquets concernant la passerelle elle-même
# - FORWARD : concernant les paquets routés
iptables -A INPUT   -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
### Règles de filtrage
### Quelques variables
PASSERELLE_DMZ="192.168.$MY_ID.1"
ROUTEUR_DMZ="192.168.$MY_ID.2"
LAN="172.16.$MY_ID.0/24"
ROUTEUR_LAN='172.16.$MY_ID.1'
IF_DMZ='eth0'
IF_LAN='eth1'
## — La passerelle d'abord
iptables -A INPUT -i $IF_DMZ -s $PASSELLE_DMZ -d $ROUTEUR_DMZ -p TCP --dport 22 -j ACCEPT
iptables -A INPUT -i $IF_DMZ -d $ROUTEUR_WAN -j DROP
iptables -A INPUT -i $IF_LAN -d $ROUTEUR_LAN -j REJECT
## — Puis les flux (aucun flux autorisé !)
iptables -A FORWARD -i $IF_LAN -d $PASSERELLE_DMZ -p UDP --dport 53 -j ACCEPT
iptables -A FORWARD -i $IF_LAN -d $PASSERELLE_DMZ -p TCP --dport 3128 -j ACCEPT
### Déni explicite par défaut
iptables -A INPUT -i $IF_DMZ -j DROP
iptables -A INPUT -i $IF_LAN -j REJECT
iptables -A FORWARD -i $IF_DMZ -j DROP
iptables -A FORWARD -i $IF_LAN -j REJECT
