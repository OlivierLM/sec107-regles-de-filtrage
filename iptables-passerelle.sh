#!/bin/bash

# Mon identifiant
MY_ID=0

### Tout Remettre à zéro
iptables -t nat -F
iptables -F
### Accepter les messages ICMP en entrée
iptables -A INPUT -p ICMP -j ACCEPT
### Tout accepter sur l'interface locale
iptables -A INPUT -i lo -j ACCEPT
### Suivi de connexions (stateful firewall)
# Sur les deux chaînes :
# - INPUT : paquets concernant la passerelle elle-même
# - FORWARD : les paquets ne sont pas routés !
iptables -A INPUT   -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
### Règles de filtrage
### Quelques variables
LAN="192.168.$MY_ID.0/24"
PASSERELLE_WAN="10.10.41.$((120 + $MY_ID))"
PASSERELLE_DMZ='192.168.$MY_ID.1'
IF_WAN='eth0'
IF_DMZ='eth1'
## — La passerelle d'abord
iptables -A INPUT -i $IF_WAN -d $PASSERELLE_WAN -p TCP --dport 22 -j ACCEPT
iptables -A INPUT -i $IF_LAN -d $PASSERELLE_LAN -p UDP --dport 53 -j ACCEPT
iptables -A INPUT -i $IF_LAN -d $PASSERELLE_LAN -p TCP --dport 3128 -j ACCEPT
## — Puis les flux (aucun flux autorisé !)
### Déni explicite par défaut
iptables -A INPUT -i $IF_WAN -j DROP
iptables -A INPUT -i $IF_DMZ -j REJECT
